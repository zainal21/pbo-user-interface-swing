/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package komponenswing_zainal;

import javax.swing.JOptionPane;

/**
 *
 * @author zainal
 */
public class FormDataSiswa extends javax.swing.JFrame {
 
    /**
     * Creates new form FormDataSiswa
     */
    public FormDataSiswa() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtalamat = new javax.swing.JTextField();
        txtnama = new javax.swing.JTextField();
        txtnis = new javax.swing.JTextField();
        cbkelas = new javax.swing.JComboBox<>();
        CbLaki = new javax.swing.JRadioButton();
        Cbperempuan = new javax.swing.JRadioButton();
        ckmenari = new javax.swing.JCheckBox();
        ckolahraga = new javax.swing.JCheckBox();
        ckgame = new javax.swing.JCheckBox();
        ckngoding = new javax.swing.JCheckBox();
        cklainnya = new javax.swing.JCheckBox();
        btnproses = new javax.swing.JToggleButton();
        btnproses1 = new javax.swing.JToggleButton();
        ckmenyanyi = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jLabel1.setText("Muhamad Zainal Arifin XIIRA 20");

        jLabel2.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        jLabel2.setText("Form Input Data Siswa");

        jLabel3.setText("NIS");

        jLabel4.setText("Nama");

        jLabel5.setText("Kelas");

        jLabel6.setText("Alamat");

        jLabel7.setText("Jenis Kelamin");

        jLabel8.setText("Hobi");

        cbkelas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "XII RA", "XII RB", "XII RC" }));

        buttonGroup1.add(CbLaki);
        CbLaki.setText("Laki-laki");

        buttonGroup1.add(Cbperempuan);
        Cbperempuan.setText("Perempuan");

        ckmenari.setText("Menari");

        ckolahraga.setText("Olahraga");

        ckgame.setText("game");

        ckngoding.setText("Ngoding");

        cklainnya.setText("Lainnya");

        btnproses.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        btnproses.setText("Proses");
        btnproses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnprosesActionPerformed(evt);
            }
        });

        btnproses1.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        btnproses1.setText("Bersihkan");
        btnproses1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnproses1ActionPerformed(evt);
            }
        });

        ckmenyanyi.setText("Menyanyi");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8)
                    .addComponent(jLabel7)
                    .addComponent(jLabel5)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addGap(36, 36, 36)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtnama)
                            .addComponent(txtnis)
                            .addComponent(txtalamat, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(CbLaki)
                                    .addComponent(cbkelas, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(10, 10, 10)
                                .addComponent(Cbperempuan)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ckolahraga)
                            .addComponent(ckgame))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ckmenari)
                            .addComponent(ckmenyanyi))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ckngoding)
                            .addComponent(cklainnya))
                        .addGap(38, 38, 38))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addComponent(btnproses, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addComponent(btnproses1, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtnama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtnis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(txtalamat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(cbkelas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(CbLaki)
                    .addComponent(Cbperempuan))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 68, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(ckolahraga)
                    .addComponent(ckmenari)
                    .addComponent(ckngoding))
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ckgame)
                    .addComponent(ckmenyanyi)
                    .addComponent(cklainnya))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnproses)
                    .addComponent(btnproses1))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(142, 142, 142))
            .addGroup(layout.createSequentialGroup()
                .addGap(160, 160, 160)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
     
    
    
//    set Joption    
    private void btnprosesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnprosesActionPerformed
        String Nama = txtnama.getText();
        String Alamat = txtalamat.getText();
        String NIS = txtnis.getText();
        String _jk = "";
        String _kelas;
        String _hobi = "";
        // jenis kelamin
        if(CbLaki.isSelected()){
            _jk = "Laki-laki"; 
        }else if(Cbperempuan.isSelected()){
            _jk = "Perempuan"; 
        }else{
         JOptionPane.showMessageDialog(null,"Data yang diinputkan tidak boleh kosong");
        }
        //kelas
        switch (cbkelas.getSelectedIndex()) {
            case 0:
                _kelas = "XII RA";
                break;
            case 1:
                _kelas = "XII RB";
                break;
            default:
                _kelas = "XII RC";
                break;
        }
        //hobi
        if(ckgame.isSelected()){
         _hobi += ckgame.getText() + ", ";
        }
        if(ckmenari.isSelected()){
         _hobi += ckmenari.getText() + ", ";
        }
        if(ckmenyanyi.isSelected()){
         _hobi += ckmenyanyi.getText() + ", ";
        }
         if(ckngoding.isSelected()){
         _hobi += ckngoding.getText() + ", ";
        }
        if(cklainnya.isSelected()){
         _hobi += cklainnya.getText() + ", ";
        }
        if(ckolahraga.isSelected()){
         _hobi += ckolahraga.getText() + ", ";
        }
        if(_hobi.equals("")){
           JOptionPane.showMessageDialog(null,"Data yang diinputkan tidak boleh kosong");
        }
        else{
           _hobi = _hobi.substring(0, _hobi.length()-2) + ".";
        }
        
        String msg =  "Nama  :  " + Nama + "\n" + 
                       "NIS :  " + NIS + "\n" +
                       "Alamat :  " + Alamat + "\n" +
                       "Kelas :  " + _kelas + "\n" +
                       "Jenis Kelamin :  " + _jk + "\n"+
                       "Hobi :  " + _hobi + "\n";
                       
        //validasi from & show message
        if(txtnama.getText().equals("") && txtalamat.getText().equals("") && txtnis.getText().equals("")){
           JOptionPane.showMessageDialog(null,"Data yang diinputkan tidak boleh kosong");
        }else{
         javax.swing.JOptionPane.showMessageDialog(this,msg, "Informasi", 
               javax.swing.JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnprosesActionPerformed

    private void btnproses1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnproses1ActionPerformed
       txtnama.setText("");
       txtalamat.setText("");
       txtnis.setText("");
       
    }//GEN-LAST:event_btnproses1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormDataSiswa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormDataSiswa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormDataSiswa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormDataSiswa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new FormDataSiswa().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton CbLaki;
    private javax.swing.JRadioButton Cbperempuan;
    private javax.swing.JToggleButton btnproses;
    private javax.swing.JToggleButton btnproses1;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cbkelas;
    private javax.swing.JCheckBox ckgame;
    private javax.swing.JCheckBox cklainnya;
    private javax.swing.JCheckBox ckmenari;
    private javax.swing.JCheckBox ckmenyanyi;
    private javax.swing.JCheckBox ckngoding;
    private javax.swing.JCheckBox ckolahraga;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtalamat;
    private javax.swing.JTextField txtnama;
    private javax.swing.JTextField txtnis;
    // End of variables declaration//GEN-END:variables
}
